class TowersOfHanoi
  attr_reader :discs, :towers
  
  def initialize(discs=3)
    @won = false
    @discs = discs
    @towers = [[*discs.downto(1)],[],[]]
  end
  
  def move(src, dst)
    @towers[dst] << @towers[src].pop if valid_move?(src, dst)
  end
  
  def valid_move?(src, dst)
    return false if ![src, dst].all? { |t| t.class == Fixnum }
    return false if @towers[src].empty?
    if !@towers[dst].empty?
      return false if @towers[src][-1] > @towers[dst][-1]
    end
    true
  end
  
  def won?
    @towers[0].empty? && 
    (@towers[1].empty? && @towers[2] == [*@discs.downto(1)]) ||
    (@towers[2].empty? && @towers[1] == [*@discs.downto(1)]) ? 
    true : false
  end
  
  def play
    get_next_move while !@won
  end
  
  private
  
  def get_next_move
    from = gets.chomp.to_i
    to = gets.chomp.to_i
    move(from, to) if valid_move?(from, to)
    @won = won?
  end
  
end
